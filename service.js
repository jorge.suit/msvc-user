/* eslint no-process-env: "off" */

const express = require('express');
const morgan = require('morgan');

const db = require('mongodb-connection-cache');
const userAuth = require('user-auth-jsonrpc');

const config = require('./config');

const app = express();

app.enable('trust proxy');

const routerRpc = new express.Router();

app.use('/', routerRpc);

if (config.get('env') !== 'test') {
  routerRpc.use(morgan('common'));
}

function mountAuth() {
  return userAuth.mount(routerRpc, {
    path: '/auth',
    db: db.get(),
    mail: config.get('mail')
  });
}

let server = null;

function startServer() {
  server = app.listen(config.get('service.port'), () => {
    console.log(`Server listening on port ${server.address().port}`);
    app.emit('ready', null);
  });
  exports.server = server;
}

function start() {
  db.connect(config.get('db.url'))
    .then(mountAuth)
    .then(startServer);
}

function close() {
  db.get().close()
    .then(() => {
      console.log('db closed');
      server.close();
    })
    .catch((err) => {
      console.log('failed closing db');
      console.log(err);
    });
}

if (process.env.NODE_ENV === 'test') {
  exports.start = start;
  exports.close = close;
  exports.app = app;
} else {
  start();
}
