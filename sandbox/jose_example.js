const jose = require('node-jose');
const keystore = jose.JWK.createKeyStore();

let key1;
keystore.generate("RSA", 1024).then(k=>{
    console.log(keystore.toJSON(false));
    console.log(k.toPEM());
    const payload = {sub: 'manolo@guateque.com'};

    jose.JWS.createSign({ format: 'compact' }, k)
	.update(JSON.stringify(payload)).final().then(x=>{console.log(x)});
    key1 = k;
});
