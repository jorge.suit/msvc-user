const jose = require('node-jose');

const key_input = {
    kty: 'RSA',
    kid: '2WVbZ-f-lElGubARp09Revr2GCKEvZNQ0IkEZhCFbqU',
    e: 'AQAB',
    n: 'grepMs3yeDvLB3cs17PBHxDeRBhvzrL-lisXbF-m1rSviEMs_2kjQrNFYl5vEGB4dcemY20J3Uoy5nsjcA7-jzIvNhD-qNSjtbGNMju5gwj3UfxFrCEi7-EHhTU5oiCRvZ5hdTZZ1-qUtfz5z5KGdmo7FA4mbTxhFalqQ2NUro0'
};

const jws_compact = 'eyJhbGciOiJSUzI1NiIsImtpZCI6IjJXVmJaLWYtbEVsR3ViQVJwMDlSZXZyMkdDS0V2Wk5RMElrRVpoQ0ZicVUifQ.eyJzdWIiOiJtYW5vbG9AZ3VhdGVxdWUuY29tIn0.RRpvdOi2kXUMh5B-8cZlW71cWS3X0Y3mEdbAZe60z3a5PGnfiPP4V0BBU6jn0hlFwhUg8NN8xqawENv4roDdmy5cd8X6CqTxojAFHt9pA56Bxr2Hn8yjmhftqpWOM8Tb6hE7nfwSRAmVJpcXsrDMYvOVQMrtFzDGRWHUeAptx5U';

jose.JWK.asKey(key_input).then(k=>{
    jose.JWS.createVerify(k)
	.verify(jws_compact).then(result=>{
	    console.log(result);
	    console.log(JSON.parse(result.payload.toString()));
	}).catch(reason=>{
	    console.log('error verifiying + ' + reason.constructor.name);
	    console.log(reason.message);
	    console.log(reason);
	});
});
