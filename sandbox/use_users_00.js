const config = require('../config');
const db = require('../tools/db');

db.connect(config.get('db.url')).then(_ => {
    console.log('db connected');
    db.get().collection('users')
	.findOne({email: 'notfound@gmail.com'})
	.then(doc => {
	    console.log(doc);
	    db.get().close().then(_ => {
		console.log('db disconnected')
	    });
	});
});
