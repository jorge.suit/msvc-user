const mailer = require('nodemailer');

let transport =  mailer.createTestAccount()
    .then(account => {
	return mailer.createTransport({
	    host: 'smtp.ethereal.email',
	    port: 587,
	    secure: false, // true for 465, false for other ports
	    auth: {
		user: account.user, // generated ethereal user
		pass: account.pass  // generated ethereal password
	    }
	});
    })
    .then(transport=>{
	console.log(Object.keys(transport));
	return transport.sendMail({
	    from: 'john.doe@domain.com',
	    to: 'jim@otherdomain.com',
	    subject: 'Nodemailer stub works!',
	    text: 'Wohoo'
	});
    })
    .then(mailer.getTestMessageUrl)
    .then(console.log);
