const config = require('../config');
const db = require('mongodb-connection-cache');
const jwks = require('jwks-db');

db.connect(config.get('db.url')).then(_=>{
    console.log('db connected');
    jwks.connect(db.get()).then(ks => {
	console.log(ks.toJSON());
	const all = ks.all({ kty: 'RSA' });
	
	console.log(all[0]);
	db.get().close().then(_=>{
	    console.log('db closed');
	});
    });
});

